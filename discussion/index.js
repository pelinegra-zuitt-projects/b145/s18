// lets create a function that will count to a series of number depending on
// the input of the user
// Get the value of the input field in order to get the value of the input using a dot(.) notation call out it's value property
// Adjacent skill to accomplish our first task


// There are 5 ways to select elements (5 DOM Selectors) 
// 1. getElementByTagName() //collective through the use of the element/tag name
// 2. getElementByClassName()  //collective
// 3. getElementById() 		//select a distinct/specific through the use of its id attribute
// 4. querySelector()  //neutral/versatile selection
// 5. querySelectorAll() 	//collective approach when selecting multiple components/elements at once.
function countUsingWhile() {
	let input1 = document.getElementById('task1').value;
	// alert(input1);
	if (input1 <= 0) {
		//inform the user that the input is not Valid.
		//innerHTML property to display
		let msg = document.getElementById('message');
		msg.innerHTML = 'Value Not Valid'; 
	} else {

	while (input1 !==0) {
		//what will happen if the condition has not been met
		alert(input1);
		input1-- //decrease the value of the input by 1 per iteration of the loop.
		}
	
	}
}


// let's create a function that will count to a series of number depending on the value inserted by the user

function countUsingDoWhile() {
	//get the input of the user.
	let number = document.getElementById('task2').value;
	// we have to make sure that the value is valid (we wont be accepting any values that are <=0)
	if (number <= 0){
		// the value is not valid
		//inform the user that he/she cannot proceed 
		// using DOM selectors we will target the container element.
		let displayText = document.getElementById('info');
		displayText.innerHTML = 'the number is Not Valid!';

	} else {
		// PROCEED because the value is VALID
		alert(number);
	}

	let indexStart = 0;
	let displayText = document.getElementById('info');
	displayText.innerHTML = number + ' is Valid';
	do {
		alert('count value: ' + indexStart);
		indexStart++
	} while (indexStart <= number);

}

// [SECTION] For Loop

// syntax: for(initialization; expression/condition; finalExpression/iteration) {
// 	//statement of procedures

// TASK: count to a series of number depending on the value inserted by the user
function countUsingForLoop() {
	//get the input from the user first using DOM selector
	let data = document.getElementById('task3').value;
	
	let res = document.getElementById('response');
	

	//validate if the value of the data is what we desire
	if (data <= 0) {
		//notify the user
		res.innerHTML = 'Input Invalid';
	} else {
		//initiatlization; condition; iteration/finalExpression
		for (let startCount = 0; startCount <= data; startCount++){
			//since the start of the count started with 0 then for every iteration we should add a
			//value of 1 to eventually meet the condition and terminate the process
		// describe what will happen per iteration
		alert('This is the value in this iteration: ' + startCount); //printout the value of each iteration before the process terminates
		}
	}
}
//[SECTION] How to access elements of a string using repetition control structures?
//get the input of the user.
//analyze the value that will be inserted by the user

function accessElementsInString() {
	// Using DOM selector, get the input of the user
	let name = document.getElementById('userName').value;
	let textLength = document.getElementById('stringlength');
	// alert(typeof  name); this is just a checker
	//validate and make sure that input is not equivalent to blank
	if (name !== '') {
		//response if truthy

		// alert('value is valid');
		// analyze the value that was inserted by the user
		// 1. Determine the length of the string.
		//=> invoke the "length" property of a string using (.) notation.
		textLength.innerHTML = 'The string is ' + name.length + ' characters long'; 
		//Upon accessing elements inside a string, this can be done so using [] square brackets.
		//keep in mind we can access each element through the use of its index number/count
//the count will start from 0 (first character inside the string corresponds to the number 0 ); the next is 1 and up to the "nth" number
		// console.log(name[0]);
		// console.log(name[1])
		// console.log(name[2])
		// console.log(name[3])
		// console.log(name[4])
		// console.log(name[5])
		// console.log(name[6])
		//We will now use the concept of loops in order to produce a much more flexible response for the user's input
		//initialization; condition; iteration
	
		 for (let startIndex = 0; startIndex < name.length; startIndex++) {
	  	 //access each element and display it inside the console
	  	 alert(name[startIndex]); 



		}

	} else {
		// response if falsy
		alert('value is Invalid')
	}
}



//how to access elements of a string? [value/ index]
//0 1 2... nth
//K a hit ano

//Behavior:if the string provided is an odd number, the middle character does not need to be checked
//dad //palindrome
//kayak // 
//we will create a loop through half of the string's characters that checks if the letters at the front and at the back of the string are the same.


// Detect if the word is a palindrome.
function detectPalindrome(){
	//1.get input using DOM selectors
	let word = document.getElementById('word').value;
	let response = document.getElementById('detectPalindrome')


	// alert(word); //checker only
	//2. Validate the data if it is indeed the correct type of info.

	if (word!=='') {
		//identify how long the word is.
		let wrdLength = word.length //
		//initialization => identify the starting point of the loop
		//condition => describes how the loop will progress and how it will be terminated
		
		//iteration --> how to advance to the next loop
		for (let index = 0; index < wrdLength / 2; index++) {
			//instruction that will happen upon each iteration of the loop
			
			//we are trying to get the current element in the string according to the index cunt
			//get the last element of the string by deducting 1 in the current length of the string since the index count of each element inside a string starts with 0
			//wrdLength -1 since our index starts with 0

			if (word[index] !== word[wrdLength - 1 - index]) {
//response		
				response.innerHTML = word +'<h3 class="text-primary"> Word is Not a Palindrome</h3> ';

			} else {
				response.innerHTML = word + '<h3 class="text-danger"> Word is a Palindrome </h3>';
			console.log(word[index] +'is the same as ' + word[wrdLength - 1 - index]);
			}
		}
	} else {

		//response
		response.innerHTML ='<h3 class="text-danger"> Value is invalid</h3> ';
	}
}
